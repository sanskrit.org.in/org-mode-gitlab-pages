* cloud storage
1. https://www.backblaze.com/b2/cloud-storage.html : Free 10 GB
2. https://icedrive.net/encrypted-cloud-storage : Free 10 GB, 3 GB Daily bandwidth
3. https://mega.io/pro
4. https://www.pcloud.com : Free 2 GB
5. https://www.sync.com/pricing-personal-free/ : Free 5 GB
6. https://www.ibm.com/cloud/free

# Sanskrit
1. Samskrit Bharati
2. https://sanskrit.nic.in
